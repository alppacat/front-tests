# Link de proyecto en producción

[Ver en navegador](https://sweet-valkyrie-1d59d2.netlify.app/)

# Teconologías usadas:

- Vue 2.6.14
- Tailwind 3.1.8
- Netlify

# ¿Cómo levantar el proyecto?

## Primeros pasos

⚠️ Crear cuenta de developer en Giphy siguiendo los [Siguientes pasos](https://developers.giphy.com/docs/api) para obter la API KEY.

- 👉 Clonar repositorio
- 👉 yarn install o npm install
- 👉 Crear documento .env dentro de ./app y colocar la variable de ambiente:
  `VUE_APP_API_KEY=API_KEY`
- 👉 yarn serve o npm serve

# ¿Cómo funciona?

La búsqueda de gifs se hace dependiendo del idioma seleccionado en el switch que se encuentra en la sidebar, el idioma por default es el español.

## Buscador

Para realizar la búsqueda solamente es necesario insertar los criterios o caractéristicas que se requieran.
Por default se buscarán gifs de perros, entonces no es necesario hacer búsquedas como `Perro corriendo` bastaría con `corriendo`.
Al mostrarse los resultados se podrá agregar a favoritos todos los gifs deseados dando click al botón de corazón ❤️.

## Barra lateral

La barra lateral cuenta con 3 secciones:

- Switch de búsqueda por idioma.
- Sección de búsquedas recientes.
- Botón accionable para ver gifs marcados como favoritos.

### Búsquedas recientes

En la sección de búsquedas recientes se mostrarán estas así como la opción de volver a visitarlas, de igual forma se podrá borrar el historial.

### Ver favoritos

El botón de ver favoritos permitirá ver todos los gifs que se han guardado en favoritos con la opción de quitarlos al accionar el botón de corazón ❤️.

## Mejoras

Las mejoras a trabajar en las próximas iteraciones serán:

- Sidebar en versión responsive.
- Navegación e interacción con la sidebar.
- Agregar paginación para ver los gifs.
- Mejor manejo de estilos condicionales con tailwind.
- Implementar TypeScript.

# Estructura

## Archivos

Dentro de `src` se encuentra el archivo index llamado `App.vue` el cual recibe los componentes creados.
Carpetas dentro de `src`

- [common](#common)
- [css](#css)
- [components](#components)

## Common

En common encontraremos el docmumento de

- `utils.js`
  El cual contiene funciones que se usarán en el proyecto.

## Css

Esta carpeta contiene el documento de

- `tailwind.css`
  El cual contiene configuración de Tailwind.

## Components

La carpeta de `components` dentro de `src` contiene cinco componentes:

- [Card.vue](#cardvue)
- [Languagebar.vue](#languagebarvue)
- [MainHeader.vue](#sainHeadervue)
- [Searchbar.vue](#searchbarvue)
- [Sidebar.vue](#sidebarvue)

### Card.vue

Props:

- `gifs: Array` : Recibe el arreglo de gifs a renderizar.
- `favorites: Array `: Recibe el arreglo los ID de los gifs añadidos a favoritos.

Methods:

- `renderGif` : Función que regresa una URL dinámica acorde al id del gif a renderizar.
- `getLike` : Función que obtiene `favorites` de las propiedades.
- `Like` : Función que emite el evento `add-favorites` para agregar un gif a favoritos acorde al ID.
- `renderLike` : Función que retorna un booleano haciendo la comparación entre los ID de los gifs y los ID añadidos a favoritos. Dependiendo de la respuesta se decide que tipo de botón de favoritos se renderiza.

Computed:

- `getGifs`: Retorna la propieda de `gifs`.

### Languagebar.vue

data:

- `es` : Booleano de idioma.
- `activeButton` : Clase para botón que se aplica dependiendo de `es`
- `unactiveButton` : Clase para botón que se aplica dependiendo de `es`

methods:

- `changeLanguage` : función toggle que reasigna el valor de `es` y emite un el evento de cambio de idioma

### MainHeader.vue

props:

- `title:String` : Modifica el título colocado.
- `icon:String` : Modifica el ícono renderizado.
- `searchbar` : Booleano para renderizar barra de búsqueda.

### Searchbar.vue

props:

- `placeholder:String` : Placeholder a recibir.

methods:

- `startSearch` : Emite un evento que será escuchado por `APP.vue` para comenzar la búsqueda. Almacena la búsqueda realizada en `localStorage`.

### Sidebar.vue

props :

- `recentSearch:Array` : Arreglo de cadenas de texto que contienen las búsquedas realizadas.
- `favorites:Array`: Arreglo que contiene las ID de los favoritos recién añadidos.

methods:

- `deleteBusquedas` : Emite evento para borrar item de búsquedas recientes en `localStorage`
- `formattedText` : Formatea la cadena de texto que se renderiza en el botón de búsquedas recientes.
- `onGetRecentSearch` : Emite evento para actualizar arreglo de búsquedas recientes.
- `onGetFavorites` : Emite evento para renderizar gifs añadidos a favoritos.
- `onGoBack:` : Emite evento para cerrar sidebar, solo para versión mobile.
