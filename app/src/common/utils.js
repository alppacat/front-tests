export const setItem = (key, value) =>
  localStorage.setItem(key, JSON.stringify(value));

export const getItem = (key) => JSON.parse(localStorage.getItem(key));

export const removeItem = (key) => localStorage.removeItem(key);

export const clearAll = () => localStorage.clear();

export const debounce = (callback, wait) => {
  let timeout;
  return (...arg) => {
    clearTimeout(timeout);
    timeout = setTimeout(() => {
      callback(...arg);
    }, wait);
  };
};
